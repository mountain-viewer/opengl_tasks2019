set(SRC_FILES
        Main.cpp
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/DebugOutput.cpp
        )

set(HEADER_FILES
        Main.h
        common/Application.hpp
        common/Camera.hpp
        common/Common.h
        common/Mesh.hpp
        common/ShaderProgram.hpp
        common/DebugOutput.h
        )


set(SHADER_FILES
        594GusarovaData1/shader.frag
        594GusarovaData1/shaderHeight.vert
)


source_group("Shaders" FILES ${SHADER_FILES}) 

include_directories(common)

MAKE_OPENGL_TASK(594Gusarova 1 "${SRC_FILES}")

if (MACOS)
    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        target_link_libraries(${TRGTNAME} "-framework CoreFoundation")
    endif()
endif()





